Un-managed Libraries Placholder
===============================

I do not put any .jar here since it makes this dist fat. You should go to the internet and fetch all .jar needed by this dist:

* TuProlog 3.0.0. Get file ``tuprolog.jar``, change into
  ``tuprolog-3.0.0.jar`` then put the file here. You may get the file
  from [TuProlog repo at bitbucket.org](https://bitbucket.org/tuprologteam/tuprolog/downloads)
