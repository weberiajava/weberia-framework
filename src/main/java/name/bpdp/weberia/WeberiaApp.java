package name.bpdp.weberia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Import;
 
import name.bpdp.weberia.service.BlazeGraph;

@Import(WeberiaConfig.class)
@SpringBootApplication
public class WeberiaApp {

    public static void main(String[] args) {

		BlazeGraph bgphService = new BlazeGraph();

		System.out.println("\n\n\n\n\n\n App name = " + bgphService.getAppName() + "\n\n\n\n\n\n");

        SpringApplication.run(WeberiaApp.class, args);

		bgphService.shutdownBlazeGraph();

    }

}
