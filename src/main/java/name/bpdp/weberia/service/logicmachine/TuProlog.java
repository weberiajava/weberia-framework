package name.bpdp.weberia.service.logicmachine;

import alice.tuprolog.*;
import alice.tuprolog.lib.*;
import alice.tuprolog.event.*;

import java.io.*;

public class TuProlog {

	static String printedResult = "";
	static String solution = "";

	public TuProlog (String theory, String goal) {

		Prolog engine = new Prolog();
		engine.addOutputListener(new OutputListener() {

			@Override
			public void onOutput(OutputEvent e) {
				printedResult += e.getMsg();
			}
		});

		try {
			engine.setTheory(new Theory(new FileInputStream(theory)));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		try {
			SolveInfo info = engine.solve(goal);
			while (info.isSuccess()) {
				solution += info.getSolution();
				if (engine.hasOpenAlternatives()) {
					info = engine.solveNext();
				} else {
					break;
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public String getPrintedResult() {
		return printedResult;
	}

	public String getSolutionResult() {
		return solution;
	}

}
