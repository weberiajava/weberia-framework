package name.bpdp.weberia.service;

import java.io.FileNotFoundException;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Properties;
import org.openrdf.repository.Repository;

import com.bigdata.rdf.sail.BigdataSailRepository;
import com.bigdata.rdf.sail.remote.BigdataSailFactory;
import com.bigdata.journal.IIndexManager;
import com.bigdata.rdf.sail.BigdataSail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
 
/*
 * @author <a href="http://bpdp.name">Bambang Purnomosidi</a>
 *
 */
@Component
public class BlazeGraph {

	@Value("${spring.application.name}")
	 private String appName;

/*
* Create or re-open a durable database instance using default
* configuration properties. There are other constructors that allow you
* to take more control over this process.
*/
	static Repository repo;

	public BlazeGraph() {
		
		//final BigdataSail sail = new BigdataSail();
		//Repository repo = new BigdataSailRepository(sail);

		repo = BigdataSailFactory.createRepository("db/bigdata.jnl");

		try {

			repo.initialize();

		} catch (Exception ex) {

			System.out.println("Problem: " + ex.getMessage());
		}

	}

	/*
	public BlazeGraph(String repoName) {

		repo = BigdataSailFactory.createRepository("db/bigdata.jnl");

		try {

			repo.initialize();

		} catch (Exception ex) {

			System.out.println("Problem: " + ex.getMessage());
		}

	}
	*/

	public void shutdownBlazeGraph() {

		try {

			repo.shutDown();

		} catch (Exception ex) {

			System.out.println("Problem: " + ex.getMessage());

		}

	}

	public String getAppName() {
		return appName;
	}

}
