![CStore](https://raw.githubusercontent.com/weberia/assets/master/images/weberia-logo.png "Weberia logo")

Weberia is a software framework for pragmatic web systems

# License

Copyright © 2015 [Bambang Purnomosidi D. P.](http://bpdp.name)

Distributed under the [Apache License - version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)
